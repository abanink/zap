ZAP
===

Zap is a full featured social network application running under the Zot6 protocol. It provides enhanced privacy modes and identity/content mirroring across multiple servers ("nomadic identity"). It does not "federate" with non-nomadic servers, protocols, or projects. 


Installation
============

Read `/install/INSTALL.txt` for installation instructions.